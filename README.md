ROS equivalents for custom Greensea LCM messages.

This is not a complete set -- merely the ones that were required by the
Raven configuration and for which existing common message definitions
could not be used.

[greensea_translator](https://gitlab.com/apl-ocean-engineering/raven/greensea_translator) provides a node that performs the translation.
